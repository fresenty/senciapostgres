<?php

require_once "conexionBD.php";

class EmpleadosM extends ConexionBD{

    //Registrar Empleados


    static public function RegistrarEmpleadosM($datosC, $tablaBD)
    {

        $pdo = ConexionBD::cBD()->prepare("INSERT INTO $tablaBD (cedula, usuario, nombres, apellidos, edad, sexo, tipo_persona) VALUES (:cedula, :usuario, :nombres, :apellidos, :edad, :sexo, 2)");
        $pdo->bindParam(":cedula", $datosC["cedula"], PDO::PARAM_STR);
        $pdo->bindParam(":usuario", $datosC["usuario"], PDO::PARAM_STR);
        $pdo->bindParam(":nombres", $datosC["nombres"], PDO::PARAM_STR);
        $pdo->bindParam(":apellidos", $datosC["apellidos"], PDO::PARAM_STR);
        $pdo->bindParam(":edad", $datosC["edad"], PDO::PARAM_STR);
        $pdo->bindParam(":sexo", $datosC["sexo"], PDO::PARAM_STR);


        if ($pdo->execute()) {

            return "Bien";

        } else {

            return "Error";
        }
        $pdo->close();
    }

        static public function RegistrarCredencialesM($datosC, $tablaBD){


        $pdo = ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD(id_cred, correo, clave, cedula ) VALUES (default, :correo, :clave, :cedula)");

        $pdo -> bindParam(":correo", $datosC["correo"], PDO::PARAM_STR);
        $pdo -> bindParam(":clave", $datosC["clave"], PDO::PARAM_STR);
        $pdo -> bindParam(":cedula", $datosC["cedula"], PDO::PARAM_STR);



        if($pdo -> execute()){

            return "Bien";

        }else {

            return "Error";
        }

        $pdo2 -> close();

    }


    //Mostrar Empleados
    static public function MostrarEmpleadosM($tablaBD){

        $pdo = ConexionBD::cBD()-> prepare("SELECT id, nombre, apellido, email, puesto, salario FROM $tablaBD");

        $pdo -> execute();

        return $pdo -> fetchAll(); // fetchALL para pedir todas las filas

        $pdo -> close();
    }

    //Editar Empleados

    static public function EditarEmpleadosM($datosC, $tablaBD){

        $pdo = ConexionBD::cBD()->prepare("SELECT id, nombre, apellido, email, puesto, salario FROM $tablaBD WHERE id = :id");

        $pdo -> bindParam(":id", $datosC, PDO::PARAM_INT);

        $pdo -> execute();

        return $pdo-> fetch();

        $pdo -> close();

    }

    //Actualizar Empleado

    static public function ActualizarEmpleadoM($datosC, $tablaBD){


        $pdo = ConexionBD::cBD()->prepare("UPDATE $tablaBD SET nombre = :nombre, apellido = :apellido, email = :email, puesto = :puesto, salario = :salario WHERE id = :id");


        $pdo -> bindParam(":id", $datosC["id"], PDO::PARAM_INT);
        $pdo -> bindParam(":nombre", $datosC["nombre"], PDO::PARAM_STR);
        $pdo -> bindParam(":apellido", $datosC["apellido"], PDO::PARAM_STR);
        $pdo -> bindParam(":email", $datosC["email"], PDO::PARAM_STR);
        $pdo -> bindParam(":puesto", $datosC["puesto"], PDO::PARAM_STR);
        $pdo -> bindParam(":salario", $datosC["salario"], PDO::PARAM_STR);

        if($pdo -> execute()){

            return "Bien";

        }else{

            return "Error";

        }

        $pdo -> close();


    }

    //Borrar empleados

    static public function BorrarEmpleadoM($datosC, $tablaBD){

        $pdo = ConexionBD::cBD()->prepare("DELETE FROM $tablaBD WHERE id = :id");

        $pdo -> bindParam(":id", $datosC, PDO::PARAM_INT);

        if($pdo -> execute()){

            return "Bien";

        }else{

            return "Error";

        }

        $pdo -> close();

    }

}