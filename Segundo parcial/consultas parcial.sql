1
(a) nombre de los ciclistas que han tenido contrato con el equipo astana (subconsulta)
	select nomb_cic from ciclistas 
	WHERE cod_cic IN (select c.cod_cic from ciclistas as c
	INNER JOIN contratos as co ON c.cod_cic = co.cod_cic
	INNER JOIN equipos as e ON co.cod_equ = e.cod_equ
	WHERE nomb_equ ='ASTANA') 
	ORDER BY cod_cic; 

(b) nombre de los ciclistas que no han tenido contrato con el equipo MOVISTAR TEAM (subconsulta)
	select cod_cic, nomb_cic from ciclistas 
	WHERE cod_cic NOT IN (select c.cod_cic from ciclistas as c
	INNER JOIN contratos as co ON c.cod_cic = co.cod_cic
	INNER JOIN equipos as e ON co.cod_equ = e.cod_equ
	WHERE nomb_equ ='MOVISTAR TEAM') 
	ORDER BY cod_cic;

l
(c) mostras los nombres y edad de los ciclistas del equipo ASTANA 
    cuya edad supera a alguna edad de los ciclistas del MOVISTAR 
    TEAM (subconsulta) 

	select nomb_cic, date_part('year',age(fechan)) from ciclistas
	WHERE cod_cic IN (select c.cod_cic from ciclistas as c
	INNER JOIN contratos as co ON c.cod_cic = co.cod_cic
	INNER JOIN equipos as e ON co.cod_equ = e.cod_equ
	WHERE e.nomb_equ ='ASTANA') 
	AND date_part('year',age(fechan)) >= ANY 
	(select MIN(date_part('year',age(fechan))) from ciclistas as c
	INNER JOIN contratos as co ON c.cod_cic = co.cod_cic
	INNER JOIN equipos as e ON co.cod_equ = e.cod_equ
	WHERE e.nomb_equ ='MOVISTAR TEAM' ); 

el ciclista con menor edad del equipo MOVISTAR es de 23 años 
por lo tanto todos los integrantes del equipo ASTANA cumplen 
con los requisitos ya que son mayores de 26 

(d) listar los nombres  de los ciclistas por equipo, estos deben estar ordenados 
	por nombre de equipo 

	select c.cod_cic, c.nomb_cic, e.nomb_equ from ciclistas as c
	INNER JOIN  contratos as co ON c.cod_cic= co.cod_cic
	INNER JOIN equipos as e ON co.cod_equ= e.cod_equ
	WHERE co.cod_equ  IN (select e.cod_equ from equipos )
	ORDER BY nomb_equ; 

(e) listar los paises, nombre de prueba y año, de los equipos que han participado 
	y han obtenido el primer puesto.

	select e.nomb_equ, n.nomb_nac, p.nomb_pru, p.year from naciones as n 
	INNER JOIN equipos as e ON n.cod_nac=e.cod_nac 
	INNER JOIN participaciones as p ON e.cod_equ= p.cod_equ
	WHERE p.puesto='1' ORDER BY year;

(f) Nombre de los equipos donde algunos de sus ciclistas ha sido campeon 
	select DISTINCT nomb_equ from equipos as e 
	INNER JOIN contratos  as co ON e.cod_equ=co.cod_equ
	INNER JOIN ciclistas as c ON co.cod_cic=c.cod_cic
	INNER JOIN pruebas as pru ON c.cod_cic=pru.ganador

utilice el DISTINCT para quitar los nombres de los equipos que se repiten








2.

(a) Restringir los valores Negativos en atributos tipo int o float

/para los datos int de la tabla pruebas
	ALTER TABLE pruebas ALTER COLUMN km SET NOT NULL; 
	ALTER TABLE pruebas ALTER COLUMN etapas SET NOT NULL; 
/para los datos int de la tabla participaciones
	ALTER TABLE participaciones ALTER COLUMN puesto SET NOT NULL; 


(b)permitir la actualización de un ciclista y hacer que estos 
   cambios se reflejen  en sus referencias
#se altera la tabla detalles
#se elimina el constrain inicial
	alter table  ciclistas drop constraint fk_na;
#se altera el constrain con la reigla de integridad
manera para permitir ('ACTUALIZACION'))
	ALTER TABLE ciclistas ADD CONSTRAINT fk_na FOREIGN KEY (cod_nac)
	REFERENCES naciones(cod_nac) ON UPDATE CASCADE;

manera para permitir actualizacion y eliminacion
	ALTER TABLE ciclistas ADD CONSTRAINT fk_na FOREIGN KEY (cod_nac)
	REFERENCES naciones(cod_nac) ON DELETE CASCADE ON UPDATE CASCADE;

#Si no especificamos RESTRICTni DELETE acción, PostgreSQL usará NO ACTION por defecto. 
#Con NO ACTION, PostgreSQL generará un error si las filas de referencia aún existen
#cuando se verifica la restricción.
#Tenga en cuenta que las acciones para eliminar también se aplican para la actualización.
#Significa que puedes tener ON UPDATE RESTRICT, ON UPDATE CASCADEy ON UPDATE NO ACTION.

(c)Permitir la eliminacion de una nacion y hacer que estos cambios 
   se refljen en sus referencias 

#se altera la tabla detalles
#se elimina el constrain inicial
	alter table  equipos drop constraint fk_nac;
#se altera el constrain con la reigla de integridad
// los cambios se le hacen a equipos ya que es la entidad que hace referencia con naciones
manera para permitir la ('ELIMINACIÓN')
	ALTER TABLE equipos ADD CONSTRAINT fk_nac FOREIGN KEY (cod_nac)
	REFERENCES naciones(cod_nac) ON DELETE CASCADE;

manera para permitir actualizacion y eliminacion
	ALTER TABLE equipos ADD CONSTRAINT fk_nac FOREIGN KEY (cod_nac)
	REFERENCES naciones(cod_nac) ON DELETE CASCADE ON UPDATE CASCADE;

(d) Realizar la validacion para que la fecha final de un contrato sea 
   mayor que la fecha final

	ALTER TABLE contratos ADD CHECK (fecfinal>fecinicio);







EXTRAS

(a1) edad y nombre de los ciclistas en orden descendentemente
	select cod_cic, nomb_cic,  MAX (date_part('year',age(fechan)))from ciclistas 
	GROUP BY nomb_cic, cod_cic ORDER BY max DESC

(a2) edad y nombre del ciclista con mayor edad
	select cod_cic, nomb_cic,  MAX (date_part('year',age(fechan)))from ciclistas 
	GROUP BY nomb_cic, cod_cic ORDER BY MAX  DESC limit 1

(b1) nombre de los ciclistas de colombia "subconsulta"
	select nomb_cic from ciclistas 
	where cod_cic IN (select c.cod_cic from ciclistas as c 
	JOIN naciones as n ON  c.cod_nac = n.cod_nac  
	WHERE nomb_nac ='Colombia')


-UNION DE 2 TABLAS CON INNER JOIN "consulta"
	select  c.nomb_cic, c.fechan, n.nomb_nac, e.nomb_equ from ciclistas as c 
	INNER JOIN naciones as n ON c.cod_nac= n.cod_nac 


-UNION DE 3 TABLAS CON INNER JOIN "consulta"
	select  c.nomb_cic, c.fechan, n.nomb_nac, e.nomb_equ from ciclistas as c 
	INNER JOIN naciones as n ON c.cod_nac= n.cod_nac 
	INNER JOIN equipos as e ON c.cod_nac= e.cod_nac

Reglas para crear una subconsulta
Ponga la subconsulta entre paréntesis.
En una subconsulta, especifique sólo una columna o expresión a no ser que esté utilizando IN, ANY, ALL o EXISTS.
Una subconsulta no puede contener una cláusula BETWEEN ni LIKE.
Una subconsulta no puede contener una cláusula ORDER BY.
Una subconsulta de una sentencia UPDATE no puede recuperar datos de la misma tabla en la que deben actualizarse los datos.
Una subconsulta de una sentencia DELETE no puede recuperar datos de la misma tabla de la que deben suprimirse los datos.













• Consultar toda la información de los ciclistas.

	select c.cod_cic, c.nomb_cic, c.fechan, e.nomb_equ, co.fecinicio, co.fecfinal 
	from ciclistas as c 
	INNER JOIN contratos as co ON c.cod_cic=co.cod_cic
	INNER JOIN equipos as e ON co.cod_equ=e.cod_equ;

• Consultar el código y nombre de los ciclistas que nacieron entre 1900 y 1999, 
  ordenados ascedentemente por nombre.

      select cod_cic, nomb_cic from ciclistas 
      WHERE fechan>'1990-01-01' AND fechan <'1999-12-31'
      ORDER BY nomb_cic ASC 


• Consultar las pruebas ciclisticas que tienen mas de 70 Kms y tienen entre 20 y 22 etapas, 
     ordenados descendentemente por prueba.

    select nomb_pru, km , etapas from pruebas 
    WHERE km > 70 and etapas>=20 and etapas<=22

• Seleccionar todos los paises que comiencen con "C".
	
	 select nomb_nac from naciones
     where nomb_nac like 'C%'
     ORDER BY nomb_nac ;

• Seleccionar todos los paises que 'NO'comiencen con "C".
	
	 select nomb_nac from naciones
     where nomb_nac NOT like 'C%'
     ORDER BY nomb_nac ;

• Seleccionar todos los ciclistas que se llamen (que lleven la cadena) "".
	
	 select nomb_cic from ciclistas
     where nomb_cic like '%Peter%'
     ORDER BY nomb_cic ;

• Consultar los nombre de los ciclistas de nacionalidad Colombiana.

	select nomb_cic from ciclistas 
	where cod_cic IN (select c.cod_cic from ciclistas as c 
	JOIN naciones as n ON  c.cod_nac = n.cod_nac  
	WHERE nomb_nac ='Colombia')


• Consultar los nombre de los ciclistas contratados entre 2014 y 2015 

	select nomb_cic from ciclistas
	WHERE cod_cic IN (select c.cod_cic from ciclistas as c
	INNER JOIN contratos as co ON c.cod_cic=co.cod_cic
	INNER JOIN equipos as e ON co.cod_equ=e.cod_equ
	WHERE  co.fecinicio>='2014-01-01' AND co.fecinicio<='2015-12-31')


• Consultar los equipos participantes en el Tour de Francia en el 2015.
	
	select e.nomb_equ from equipos as e
	INNER JOIN participaciones as p ON e.cod_equ=p.cod_equ
	WHERE p.nomb_pru='Tour de Francia' AND p.year ='2015'

• Consultar los ciclistas que ganaron la vuelta a España entre 2014 y 2017,
 informando ademas a que equipo pertenecian en ese tiempo.

 	select c.cod_cic ,c.nomb_cic from ciclistas as c
	INNER JOIN pruebas as pru ON c.cod_cic=pru.ganador
	WHERE pru.nomb_pru='Vuelta a España' AND pru.year >='2015' AND pru.year<='2017'

• Consultar el puesto que ocupo el equipo MOVISTAR en el Tour de Francia en el año 2015.

	select p.puesto from equipos as e
	INNER JOIN participaciones as p ON e.cod_equ=p.cod_equ
	WHERE p.nomb_pru='Tour de Francia' AND p.year ='2015'
