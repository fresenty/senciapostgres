<?php 

session_start();

if(!$_SESSION["Ingreso"]){

	header("location:index.php?ruta=ingreso");

	exit();
}

 ?>
	<br>
	<h1>Empleados</h1>

	<table id="t1" border="1">
		
		<thead>
			
			<tr>
				<th>Cedula</th>
				<th>Usuario</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Edad</th>
				<th>Sexo</th>
				<th></th>
				<th></th>

			</tr>

		</thead>

		<tbody>
			


        <?php
        $mostrar = new EmpleadosC();
        $mostrar -> MostrarEmpleadosC();

        ?>

		</tbody>

	</table>

    <?php

    $eliminar = new EmpleadosC();
    $eliminar -> BorrarEmpleadoC();

