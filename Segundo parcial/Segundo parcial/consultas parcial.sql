(a) nombre de los ciclistas que han tenido contrato con el equipo astana (subconsulta)
select nomb_cic from ciclistas 
WHERE cod_cic IN (select c.cod_cic from ciclistas as c
INNER JOIN contratos as co ON c.cod_cic = co.cod_cic
INNER JOIN equipos as e ON co.cod_equ = e.cod_equ
WHERE nomb_equ ='ASTANA') 
ORDER BY cod_cic 

(b) nombre de los ciclistas que no han tenido contrato con el equipo MOVISTAR TEAM (subconsulta)
select cod_cic, nomb_cic from ciclistas 
WHERE cod_cic NOT IN (select c.cod_cic from ciclistas as c
INNER JOIN contratos as co ON c.cod_cic = co.cod_cic
INNER JOIN equipos as e ON co.cod_equ = e.cod_equ
WHERE nomb_equ ='MOVISTAR TEAM') 
ORDER BY cod_cic 


(c) mostras los nombres y edad de los ciclistas del equipo ASTANA 
    cuya edad supera a alguna edad de los ciclistas del MOVISTAR 
    TEAM (subconsulta) 

    //"BUSCAR"

(d) listar los nombres  de los ciclistas por equipo, estos deben estar ordenados 
	por nombre de equipo 

select c.cod_cic, c.nomb_cic, e.nomb_equ from ciclistas as c
INNER JOIN  contratos as co ON c.cod_cic= co.cod_cic
INNER JOIN equipos as e ON co.cod_equ= e.cod_equ
WHERE co.cod_equ  IN (select e.cod_equ from equipos )
ORDER BY nomb_equ 

(e) listar los paises, nombre de prueba y año, de los equipos que han participado 
	y han obtenido el primer puesto.





EXTRAS

(a1) edad y nombre de los ciclistas en orden descendentemente
select MAX (date_part('year',age(fechan))), nomb_cic from ciclistas 
GROUP BY nomb_cic ORDER BY max DESC




--nombre de los ciclistas de colombia "consulta y subconsulta"


select nomb_cic from ciclistas 
where cod_nac =(select cod_nac from naciones where cod_nac= '41' )


-UNION DE 2 TABLAS CON INNER JOIN "consulta"
select  c.nomb_cic, c.fechan, n.nomb_nac, e.nomb_equ from ciclistas as c 
INNER JOIN naciones as n ON c.cod_nac= n.cod_nac 


-UNION DE 3 TABLAS CON INNER JOIN "consulta"
select  c.nomb_cic, c.fechan, n.nomb_nac, e.nomb_equ from ciclistas as c 
INNER JOIN naciones as n ON c.cod_nac= n.cod_nac 
INNER JOIN equipos as e ON c.cod_nac= e.cod_nac

Reglas para crear una subconsulta
Ponga la subconsulta entre paréntesis.
En una subconsulta, especifique sólo una columna o expresión a no ser que esté utilizando IN, ANY, ALL o EXISTS.
Una subconsulta no puede contener una cláusula BETWEEN ni LIKE.
Una subconsulta no puede contener una cláusula ORDER BY.
Una subconsulta de una sentencia UPDATE no puede recuperar datos de la misma tabla en la que deben actualizarse los datos.
Una subconsulta de una sentencia DELETE no puede recuperar datos de la misma tabla de la que deben suprimirse los datos.




    • Consultar toda la información de los ciclistas.
    • Consultar el código y nombre de los ciclistas que nacieron entre 1900 y 1999, ordenados ascedentemente por nombre.
    • Consultar las pruebas ciclisticas que tienen mas de 70 Kms y tienen entre 5 y 10 etapas, ordenados descendentemente por prueba.
    • Seleccionar todos los paises que comiencen con "C".

    • Consultar los nombre de los ciclistas de nacionalidad Colombiana.

    • Consultar los nombre de los ciclistas contratados entre 2001 y 2003 del equipo MOVISTAR.

    • Consultar los equipos participantes en el Tour de Francia en el 2010.

    • Consultar los ciclistas que ganaron la vuelta a España entre 1987 y 2005, informando ademas a que equipo pertenecian en ese tiempo.

    • Consultar los ciclistas Franceses que participaron en la Vuelta a España en el 2005.
    • Consultar el puesto que ocupo el equipo MOVISTAR en el Tour de Francia en el año 2001.
