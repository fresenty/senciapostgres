<?php

class EmpleadosC
{

    //Registrar los Estudiantes

    public function RegistrarEmpleadosC()
    {

        if (isset($_POST["cedulaR"])) {  //si la variable viene con registro

            $datosC = array("cedula" => $_POST["cedulaR"], "usuario" => $_POST["usuarioR"], "nombres" => $_POST["nombresR"], "apellidos" => $_POST["apellidosR"], "edad" => $_POST["edadR"], "sexo" => $_POST["sexoR"]);

            $tablaBD = "persona";

            $respuesta = EmpleadosM::RegistrarEmpleadosM($datosC, $tablaBD);

            if ($respuesta == "Bien") {

                header("location:index.php?ruta=empleados");

            } else {

                echo "error";
            }

        }

    }

    //Registrar los Credenciales

    public function RegistrarCredencialesC()
    {

        if (isset($_POST["cedulaR"])) {  //si la variable viene con registro

            $datosC = array("cedula" => $_POST["cedulaR"], "correo" => $_POST["correoR"], "clave" => $_POST["claveR"]);

            $tablaBD = "credenciales";

            $respuesta = EmpleadosM::RegistrarCredencialesM($datosC, $tablaBD);

            if ($respuesta == "Bien") {

                header("location:index.php?ruta=empleados");

            } else {

                echo "error";
            }

        }

    }

    //Mostrar Empleados
    public function MostrarEmpleadosC()
    {

        $tablaBD = "empleados";

        $respuesta = EmpleadosM::MostrarEmpleadosM($tablaBD);

        foreach ($respuesta as $key => $value) {

            echo '<tr>
					<td>' . $value["nombre"] . '</td>
					<td>' . $value["apellido"] . '</td>
					<td>' . $value["email"] . '</td>
					<td>' . $value["puesto"] . '</td>
					<td>$ ' . $value["salario"] . '</td>
					<td><a href="index.php?ruta=editar&id=' . $value["id"] . '"><button>Editar</button></a></td>
				    <td><a href="index.php?ruta=empleados&idB=' . $value["id"] . '"><button>Borrar</button></a></td>
			      </tr>';

        }

    }

    //Editar Empleado

    public function EditarEmpleadosC()
    {

        $datosC = $_GET["id"];
        $tablaBD = "empleados";

        $respuesta = EmpleadosM::EditarEmpleadosM($datosC, $tablaBD);

        echo '<input type="hidden" value="' . $respuesta["id"] . '" name="idE">

		  <input type="text" placeholder="Nombre" value="' . $respuesta["nombre"] . '" name="nombreE" required>

   		  <input type="text" placeholder="Apellido" value="' . $respuesta["apellido"] . '" name="apellidoE" required>
 
		  <input type="email" placeholder="Email" value="' . $respuesta["email"] . '" name="emailE" required>

		  <input type="text" placeholder="Puesto" value="' . $respuesta["puesto"] . '" name="puestoE" required>

		  <input type="text" placeholder="Salario" value="' . $respuesta["salario"] . '" name="salarioE" required>

		  <input type="submit" value="Actualizar">';
    }

    //Actualizar Empleado

    public function ActualizarEmpleadoC()
    {

        if (isset($_POST["nombreE"])) {

            $datosC = array("id" => $_POST["idE"], "nombre" => $_POST["nombreE"], "apellido" => $_POST["apellidoE"], "email" => $_POST["emailE"], "puesto" => $_POST["puestoE"], "salario" => $_POST["salarioE"]);


            $tablaBD = "empleados";
            $respuesta = EmpleadosM::ActualizarEmpleadoM($datosC, $tablaBD);


            if ($respuesta == "Bien") {

                header("location:index.php?ruta=empleados");

            } else {

                echo "error";


            }
        }
    }

    //Eliminar Empleados

    public function BorrarEmpleadoC(){


        if (isset($_GET["idB"])) {

            $datosC = $_GET["idB"];

            $tablaBD = "empleados";

            $respuesta = EmpleadosM::BorrarEmpleadoM($datosC, $tablaBD);

            if ($respuesta == "Bien") {

                header("location:index.php?ruta=empleados");

            } else {

                echo "error";


            }

        }

    }
}