CREATE TABLE naciones (
	cod_nac varchar(5),
	nomb_nac varchar(50),
	capital varchar(50)
);
ALTER TABLE naciones ADD CONSTRAINT p1key PRIMARY KEY(cod_nac);
CREATE TABLE ciclistas (
	cod_cic varchar(5),
	nomb_cic varchar(30),
	fechan date,
	cod_nac varchar(5)
);
ALTER TABLE ciclistas ADD CONSTRAINT pkc PRIMARY KEY(cod_cic);
CREATE TABLE pruebas (
	nomb_pru varchar(20),
	year int4,
	km int4,
	etapas int4,
	ganador varchar(5)
);
ALTER TABLE pruebas ADD CONSTRAINT pkp1 PRIMARY KEY(nomb_pru,year);
CREATE TABLE participaciones (
	nomb_pru varchar(20),
	year int4,
	cod_equ varchar(5),
	puesto int4
);
ALTER TABLE participaciones ADD CONSTRAINT pk PRIMARY KEY(nomb_pru,year,cod_equ);
CREATE TABLE equipos (
	cod_equ varchar(5),
	director varchar(30),
	cod_nac varchar(5),
	nomb_equ varchar(30)
);
ALTER TABLE equipos ADD CONSTRAINT pke PRIMARY KEY(cod_equ);
CREATE TABLE contratos (
	cod_cic varchar(5),
	cod_equ varchar(5),
	fecinicio date,
	fecfinal date
);

//relaciones de contratos 
ALTER TABLE contratos ADD CONSTRAINT pk2 PRIMARY KEY(cod_cic,cod_equ); // on linea para modifacar un constraint y agregar una llave primaria extra.
ALTER TABLE contratos ADD CONSTRAINT fk_cic FOREIGN KEY (cod_cic) REFERENCES ciclistas(cod_cic) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE contratos ADD CONSTRAINT fk_eq FOREIGN KEY (cod_equ) REFERENCES equipos(cod_equ) ON DELETE NO ACTION ON UPDATE NO ACTION;

//relaciones de participaciones
ALTER TABLE participaciones ADD CONSTRAINT fk_equ FOREIGN KEY (cod_equ) REFERENCES equipos(cod_equ) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE participaciones ADD CONSTRAINT fk_pr FOREIGN KEY (nomb_pru,year) REFERENCES pruebas(nomb_pru,year) ON DELETE NO ACTION ON UPDATE NO ACTION;

//relaciones de pruebas
ALTER TABLE pruebas ADD CONSTRAINT fk_ga FOREIGN KEY (ganador) REFERENCES ciclistas(cod_cic) ON DELETE NO ACTION ON UPDATE NO ACTION;

//relaciones de ciclistas
ALTER TABLE ciclistas ADD CONSTRAINT fk_na FOREIGN KEY (cod_nac) REFERENCES naciones(cod_nac) ON DELETE NO ACTION ON UPDATE NO ACTION;

//relaciones de equipos
ALTER TABLE equipos ADD CONSTRAINT fk_nac FOREIGN KEY (cod_nac) REFERENCES naciones(cod_nac) ON DELETE NO ACTION ON UPDATE NO ACTION;



 COPY naciones(cod_nac,nomb_nac,capital) FROM '/home/fredy/Segundo parcial/naciones.csv' DELIMITER ',' CSV HEADER;  
 COPY ciclistas(cod_cic,nomb_cic,fechan,cod_nac) FROM '/home/fredy/Segundo parcial/ciclistas.csv' DELIMITER ',' CSV HEADER; 
 COPY equipos(cod_equ,nomb_equ,cod_nac,director) FROM '/home/fredy/Segundo parcial/equipos.csv' DELIMITER ',' CSV HEADER;  
 COPY contratos(cod_cic,cod_equ,fecinicio,fecfinal) FROM '/home/fredy/Segundo parcial/contratos.csv' DELIMITER ',' CSV HEADER;
 COPY pruebas(nomb_pru,year,km,etapas,ganador) FROM '/home/fredy/Segundo parcial/pruebas.csv' DELIMITER ',' CSV HEADER; 
 COPY participaciones(nomb_pru,year,cod_equ,puesto) FROM '/home/fredy/Segundo parcial/participaciones.csv' DELIMITER ',' CSV HEADER; 


select * from naciones

select * from ciclistas

select * from equipos	

select * from participaciones

select * from contratos

select * from pruebas




2030
2067
2049

https://www.academia.edu/32007759/Pr%C3%A1ctica_3_Lenguaje_SQL_1a_Parte_Manipulaci%C3%B3n_de_Bases_de_Datos?auto=download


select puesto from participaciones where puesto ='1'